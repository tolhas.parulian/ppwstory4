from django.urls import path
from . import views

app_name = 'landingpage'

urlpatterns = [
    path('', views.intro, name='intro'),
    path('mywriting/', views.my_writing, name='my_writing'),
    path('myexperiences/', views.my_experiences, name='my_experiences'),
    path('myresumecontact/', views.my_resume_contact, name='my_resume_contact'),
]